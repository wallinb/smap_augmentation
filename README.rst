SMAP L3 HDF5 file usability augmentations
+++++++++++++++++++++++++++++++++++++++++

Currently, the SMAP L3 granules have some usability limitations with respect to
spatial metadata and relationships between variables, coordinates, and
dimensions:

* Importing into GIS (QGIS, ArcGIS) is difficult, generally requiring manual
  specification of grid and projection parameters (which are not readily
  availible in granule metadata).  
* The OPeNDAP HDF handler cannot represent
  shared dimensions or relationships to coordinate variables for these data.  
* The compliance to CF conventions for coordinates and spatial relationships is
  incomplete or contrary to recommendations and community practices:

  + geographic coordinate variables only partially adhere to CF 1.7 
  + native projection and projection coordinates missing


CF Related Issues
=================

These are specific issues that seem to be indicated by the `CF Conventions
v1.7`_:


Issue 1 - Missing "grid mapping" definition and native regular grid coordinates
-------------------------------------------------------------------------------

The native coordinates are on the EASE 2.0 Global regular grid, yet coordinates
in this system are not included in the granule. Furthermore, CF has a mechanism
to encode the mapping from latitude/longitude to native coordinates and for
relating that mapping to variables and coordinates (see `CF 1.7 section 5.6`_)
which is absent.


Issue 2 - Incorrect format of latitude and longitude coordinate variables
-------------------------------------------------------------------------

Latitude and longitude should be encoded as "coordinate variables" in the
terminology of CF, not "auxillary coordinate variables".  

From `CF 1.7 section 5`_:

    "Any of a variable’s dimensions that is an independently varying latitude,
    longitude, vertical, or time dimension (see Section 1.2, "Terminology") and
    that has a size greater than one must have a corresponding coordinate
    variable, i.e., a one-dimensional variable with the same name as the
    dimension (see examples in Chapter 4, Coordinate Types). This is the only
    method of associating dimensions with coordinates that is supported by
    [COARDS].

    ...

    If the longitude, latitude, vertical or time coordinate is multi-valued,
    varies in only one dimension, and varies independently of other
    spatiotemporal coordinates, it is not permitted to store it as an auxiliary
    coordinate variable."


Other Issues
============

Issue 3 - Missing dimension scales
----------------------------------

The HDF5 'dimension scale' mechanism, which encodes the association between
dependent data variables and independent coordinate variables, is not
implemented for SMAP granules.  This is how labels can be attached to
dimensions, and dimension variables linked to associated datasets in the HDF5
data model. 

The NASA Data Interoperability Working Group recommends these be implemented to
be compatible with the NetCDF4 data model (which is HDF5 "under the hood"). See
`Data Interoperability Working Group recommendation (ESDS-RFC- 028v1.1) section
2.8`_ section 2.8 and the series of posts by John Caron on the `Unidata
developer's blog`_.

Issue 4 - Flag values in coordinate variables
---------------------------------------------

Flag values are included in latitude/longitude variables.  This does not appear
to be addressed by CF, but is a hinderance to their use and interpretation.
Those grid locations with flag values in their latitude/longitude coordinate
grids DO have valid latitude and longitude values, it is just that the
measurements themselves are flagged at those locations.

Further complicating this is that attributes which would indicate the use and
meaning of flag values is missing for the latitude and longitude variables
(e.g. 'flag_values', 'flag_meanings').


Sample Augmentation
===================

A number of additive changes could be made to SMAP L3 files which would largely
address these issues with minimal impact to backwards compatibility:

* Add '/Soil_Moisture_Retrieval_Data/x' and '/Soil_Moisture_Retrieval_Data/y'
  coordinate variables

  + containing 1D arrays of EASE 2.0 global grid coordinate values + populate
    CF attributes: axis, units, standard_name, long_name + use HDF5 'dimension
    scales' mechanism to associate these coordinate variables with the
    corresponding dimensions on other variables

* Similarly, add '/Soil_Moisture_Retrieval_Data/landcover_rank' coordinate
  variable

  + containing 1D array with GLCF landcover class rank (e. g. [1, 2, 3]) + use
    HDF5 'dimension scales' mechanismn to associate with the 3rd dimension on
    the landcover class variables

* Add grid mapping variable
  '/Soil_Moisture_Retrieval_Data/EASE2_global_projection'

  + populate 'grid_mapping' attribute on gridded variables associated with this
    mapping

These changes are demonstrated with the sample augmented SMAP_L3_SM_P file
linked below:

    `SMAP_L3_SM_P_20151012_R11920_001.augmented.h5
    <https://drive.google.com/open?id=1g_HV0zLoUj6qs4-i_8OPZH_8pHt9TvkQ>`_

This addresses issues 1 and 3, with a compromise struck for Issue 2. To fully
address Issue 2 would require changing the shape of the 'latitude' and
'longitude' variables which could have greater impact on existing users. Issue
4 is somewhat orthogonal to the rest and not addressed here. 


Effect of Remediations
======================

Programmatic access
-------------------

Granule native projection and grid parameters/coordinates can be directly
extracted or easily calculated from the granule itself. This more complete
characterization of the raster data facilitates its use and interpretation.

ArcGIS
------

Based on communication with ESRI development group, these changes are
beneficial to their ongoing efforts to make SMAP granules more directly
accessible in ArcGIS products.

QGIS
----

Manual setup for projection/grid definition still required to import data as
raster.  QGIS uses GDAL to read HDF5, which only supports HDF-EOS5 conventions
at this time.

OPeNDAP
-------

Implementation
==============

The :code:`augment_smap.py` python script applies the proposed changes to a sample SMAP L3 SM P granule.

Simply run :code:`python augment_smap.py` to generate the augmented granule.

Requirements
------------

Tested with:

* python=3.6.4
* h5py=2.7.1
* numpy=1.13.3
* toolz=0.8.2
* cartopy=0.15.1

Dimensions are named and associated to variables appropriately according to the
DAP3 data model.

.. _CF Conventions v1.7:
   http://cfconventions.org/Data/cf-conventions/cf-conventions-1.7/cf-conventions.html
.. _CF 1.7 section 5:
   http://cfconventions.org/Data/cf-conventions/cf-conventions-1.7/cf-conventions.html#coordinate-system
.. _CF 1.7 section 5.6:
   http://cfconventions.org/Data/cf-conventions/cf-conventions-1.7/cf-conventions.html#grid-mappings-and-projections
.. _Unidata developer's blog:
   https://www.unidata.ucar.edu/blogs/developer/entry/dimensions_scales
.. _Data Interoperability Working Group recommendation (ESDS-RFC- 028v1.1) section 2.8:
   https://cdn.earthdata.nasa.gov/conduit/upload/5098/ESDS-RFC-028v1.1.pdf
