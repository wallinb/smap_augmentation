'''
SMAP L3_SM_P augmentation script

This script performs a set of augmentations to a SMAP L3_SM_P granule designed
to improve usability by:

1. Improvements to adherence of CF 1.7 conventions
2. Addition of "dimension scales" and native coordinate variables

Author: Bruce Wallin
Email: bruce.wallin@nsidc.org
'''
import shutil
from pathlib import Path

import h5py
import numpy as np
from toolz import first
from cartopy.crs import Projection, sgeom

GRANULE_FILEPATH = 'SMAP_L3_SM_P_20151012_R11920_001.h5'
AUGMENTED_FILEPATH = 'SMAP_L3_SM_P_20151012_R11920_001.aug.h5'

ROOT_PATH = Path('/Soil_Moisture_Retrieval_Data')


def flatten_array(arr, axis, flag_value=-9999):
    '''
    Given an array that is redundant along 'axis', flatten to remove that axis
    ignoring 'flag_value' values.
    '''
    def flatten_row(row):
        values = set(row)
        values.discard(flag_value)
        assert len(values) == 1
        return values.pop()

    flattened = np.apply_along_axis(flatten_row, axis, arr)

    return flattened


class EASE2_global(Projection):
    def __init__(self):
        proj4_params = [
            ('proj', 'cea'),
            ('lat_ts', 30),
            ('lon_0', 0),
            ('units', 'm'),
            ('x_0', 0),
            ('y_0', 0)
        ]
        super(EASE2_global, self).__init__(proj4_params=proj4_params, globe=None)

    @property
    def boundary(self):
        coords = ((self.x_limits[0], self.y_limits[0]),
                  (self.x_limits[1], self.y_limits[0]),
                  (self.x_limits[1], self.y_limits[1]),
                  (self.x_limits[0], self.y_limits[1]),
                  (self.x_limits[0], self.y_limits[0]))

        return sgeom.Polygon(coords).exterior

    @property
    def threshold(self):
        return 1e5

    @property
    def x_limits(self):
        return (-17367530.445161376, 17367530.445161376)

    @property
    def y_limits(self):
        return (-7314540.08900637, 7314540.08900637)


def associate_dim_scale(f, var, dim_path, dim_idx):
    '''
    Create and associate dimension scale to 'dim_path' on the 'dim_idx' indexed
    dimension of 'var' in dataset 'f'.
    '''
    var.dims.create_scale(f[str(dim_path)], dim_path.name)
    var.dims[dim_idx].label = dim_path.name
    var.dims[dim_idx].attach_scale(f[str(dim_path)])


def add_native_coordinate_variables(f):
    '''
    Add native coordinate variables to dataset 'f'
    '''
    ease = EASE2_global()
    wgs84 = ease.as_geodetic()

    lons = flatten_array(f[str(ROOT_PATH)]['longitude'], axis=0)
    lats = flatten_array(f[str(ROOT_PATH)]['latitude'], axis=1)

    x = ease.transform_points(src_crs=wgs84, x=lons, y=np.full(lons.shape, lats[0]))[:, 0]
    y = ease.transform_points(src_crs=wgs84, x=np.full(lats.shape, lons[0]), y=lats)[:, 1]

    f[str(ROOT_PATH)]['x'] = x
    f[str(ROOT_PATH)]['y'] = y

    f[str(ROOT_PATH)]['x'].attrs['standard_name'] = 'projection_x_coordinate'
    f[str(ROOT_PATH)]['y'].attrs['standard_name'] = 'projection_y_coordinate'

    f[str(ROOT_PATH)]['x'].attrs['axis'] = 'X'
    f[str(ROOT_PATH)]['y'].attrs['axis'] = 'Y'

    f[str(ROOT_PATH)]['x'].attrs['units'] = 'm'
    f[str(ROOT_PATH)]['y'].attrs['units'] = 'm'

    f[str(ROOT_PATH)]['x'].attrs['long_name'] = 'X coordinate of cell center in EASE 2.0 global projection'
    f[str(ROOT_PATH)]['y'].attrs['long_name'] = 'Y coordinate of cell center in EASE 2.0 global projection'


def add_landcover_coordinate_variable(f):
    '''
    Add coordinate variable for landcover ordering.
    '''
    f[str(ROOT_PATH)]['landcover_rank'] = np.array([1, 2, 3])


def add_grid_mapping_variable(f):
    '''
    Add CF grid mapping variable for EASE 2.0 global projection.
    '''
    projection_var = 'EASE2_global_projection'
    f[str(ROOT_PATH)][projection_var] = ''
    f[str(ROOT_PATH)][projection_var].attrs['grid_mapping_name'] = 'lambert_cylindrical_equal_area'
    f[str(ROOT_PATH)][projection_var].attrs['standard_parallel'] = 30.0
    f[str(ROOT_PATH)][projection_var].attrs['false_easting'] = 0.0
    f[str(ROOT_PATH)][projection_var].attrs['false_northing'] = 0.0
    f[str(ROOT_PATH)][projection_var].attrs['false_northing'] = 0.0
    f[str(ROOT_PATH)][projection_var].attrs['longitude_of_central_meridian'] = 0


def augment():
    '''
    Perform augmentation on sample file.
    '''
    shutil.copyfile(GRANULE_FILEPATH, AUGMENTED_FILEPATH)
    with h5py.File(AUGMENTED_FILEPATH, 'r+') as f:
        expected_shapes = {
            ('y', 'x'): (406, 964),
            ('y', 'x', 'landcover_rank'): (406, 964, 3)
        }

        # Add ease grid coordinates
        add_native_coordinate_variables(f)
        add_landcover_coordinate_variable(f)
        add_grid_mapping_variable(f)

        # Attach ease grid coordinates as dimension scales to other variables
        variable_paths = [ROOT_PATH / var_name for var_name in f[str(ROOT_PATH)]]
        for var_path in variable_paths:
            var = f[str(var_path)]
            try:
                dims_match = first(dims
                                   for dims, shape in expected_shapes.items()
                                   if var.shape == shape)
            except StopIteration:
                pass
            else:
                var.attrs['grid_mapping'] = 'EASE2_global_projection'
                for dim_idx, dim_name in enumerate(dims_match):
                    dim_path = ROOT_PATH / dim_name
                    associate_dim_scale(f, var, dim_path, dim_idx)

        # Add optional attributes to coordinate variables
        f[str(ROOT_PATH)]['latitude'].attrs['standard_name'] = 'latitude'
        f[str(ROOT_PATH)]['longitude'].attrs['standard_name'] = 'longitude'


augment()
